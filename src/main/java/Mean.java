import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Mean implements InterfaceCount {

    private final List<Integer> listNilai = new ArrayList<>();
    private final ReadAndConvertToList convert = new ReadAndConvertToList();

    public String hitung() throws IOException {

        convert.convertToList(listNilai);

        float total = 0;
        for (int t : listNilai){
            total = total + t;
        }
        float hasil = total/ listNilai.size();
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(hasil);
    }
}
