import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteFrekuensi extends AbstractWrite {

    private final Frekuensi frekuensi = new Frekuensi();

    @Override
    public void tulis() {
        try {
            File file = new File("C:\\Users\\donit\\Downloads\\Challenge3\\Challenge3\\src\\file\\data_sekolah_modus.txt");
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write (frekuensi.hitung());
            bwr.newLine();
            bwr.flush();
            bwr.close();
            if (pilihan.equals("1")){
                generate(output1);
            }
        }
        catch (IOException ioe){
            if (pilihan.equals("1")) {
                menu.menuTop();
                System.out.print(ioe);
                menu.failedGenerate();
            }
        }
    }
}
