import java.io.*;
import java.util.List;

public class ReadAndConvertToList {

    void convertToList(List<Integer> list) throws IOException {

        File file = new File("C:\\Users\\donit\\Downloads\\Challenge3\\Challenge3\\src\\file\\data_sekolah.csv");
        if (!file.exists()) {
            throw new FileNotFoundException ("""

                    File tidak ditemukan. Pastikan anda telah meletakkan file csv
                    dengan nama file data_sekolah.csv di direktori berikut:
                    C:\\Users\\donit\\Downloads\\Challenge3\\Challenge3\\src\\file
                    """);
        }
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        String[] tempArr;
        while ((line = br.readLine()) != null) {
            tempArr = line.split(";");
            for (int i = 1; i < tempArr.length; i++) {
                list.add(Integer.parseInt(tempArr[i]));
            }
        }
        br.close();
    }
}
