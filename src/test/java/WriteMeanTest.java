import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteMeanTest {

    private final Mean mean = new Mean();
    private final Median median = new Median();
    private final Modus modus = new Modus();

    @Test
    @DisplayName("Test write mean atau rata-rata")

    void testWriteMean (){

        AbstractWrite testwritemean = new AbstractWrite() {
            @Override
            void tulis() {
                try {
                    File file = new File("C:\\Users\\donit\\Downloads\\Challenge3\\Challenge3\\src\\file\\data_sekolah_modus_median.txt");
                    FileWriter writer = new FileWriter(file);
                    BufferedWriter bwr = new BufferedWriter(writer);
                    bwr.write ("Berikut Hasil Pengolahan Nilai:\n\n");
                    bwr.write("Berikut hasil sebaran data nilai:\n");
                    bwr.write("Mean: " + mean.hitung()+"\n");
                    bwr.write("Median: "+ median.hitung()+"\n");
                    bwr.write("Modus: "+ modus.hitung());
                    bwr.newLine();
                    bwr.flush();
                    bwr.close();
                    if (pilihan.equals("3")){
                        generate(output1,output2);
                    } else {
                        generate(output2);
                    }
                }
                catch (IOException ioe){
                    menu.menuTop();
                    System.out.print(ioe);
                    menu.failedGenerate();
                }
                finally {
                    menu.back();
                }
            }
        };
    }
}
